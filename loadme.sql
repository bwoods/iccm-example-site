CREATE TABLE people (
	id int NOT NULL AUTO_INCREMENT,
	last_name varchar(255) NOT NULL,
	first_name varchar(255) NOT NULL
);
INSERT INTO people ( last_name, first_name) VALUES ('Smith', 'John');
